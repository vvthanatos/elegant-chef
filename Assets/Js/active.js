$(document).ready(function() {
	
	/*
		Aleksey Skubaev

		askubaev@gmail.com
		icq - 322253350
		Разработка шаблонов для DLE и кроссбраузерная верстка
		------------------
		Необходимые jQuery скрипты.
	
	*/
	
	//slaider//
	
	
	$('.slaider-block-all:first').addClass('slaider-activ').find('.slaider-block-news').fadeIn();
	$('.slaider-block-all:first').find('.slaider-block-news-mini').addClass('mini-activ');
	
	
	function showNext() {
		clearTimeout(timerId);
		var currentBlock = $('.slaider-activ');
		var nextBlock = currentBlock.next('.slaider-block-all').length ? currentBlock.next('.slaider-block-all') : $('.slaider-block-all:first');
		currentBlock.find('.slaider-block-news').fadeOut();
		$('.slaider-block-all').removeClass('slaider-activ');
		nextBlock.addClass('slaider-activ').find('.slaider-block-news').fadeIn();
		currentBlock.find('.slaider-block-news-mini').removeClass('mini-activ');
		nextBlock.find('.slaider-block-news-mini').addClass('mini-activ');
		timerId = setTimeout(showNext, 8000);
	};
	
	timerId = setTimeout(showNext, 8000);

	
	$('.slaider-block-news-mini').click(function() {
		clearTimeout(timerId);
		$('.slaider-block-all').removeClass('slaider-activ');
		$('.slaider-block-news').fadeOut();
		$(this).parent().addClass('slaider-activ').find('.slaider-block-news').fadeIn();
		$(this).parent().addClass('slaider-activ').find('.slaider-block-news').fadeIn();
		$('.slaider-block-all').find('.slaider-block-news-mini').removeClass('mini-activ');
		$(this).parent().addClass('slaider-activ').find('.slaider-block-news-mini').addClass('mini-activ');
		
		
	});

	
	//-slaider//
	
	$('.slaider-block-news-title:first').css({'background':'none'});
	
	$('#foo1').carouFredSel({
		prev: '#prev1',
		next: '#next1',
		scroll: 1,
		auto: false
	});
	
	$('#foo2').carouFredSel({
		prev: '#prev2',
		next: '#next2',
		height: 440,
		scroll: 1,
		auto: false
	});
	
	$('#foo3').carouFredSel({
		prev: '#prev3',
		next: '#next3',
		scroll: 1,
		auto: false
	});
	
	$('.comment-text-more input').click(function() {
		$('.mass_comments_action').show();
	});
	
});
