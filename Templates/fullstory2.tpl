<script type="text/javascript">
	$(function(){
		$('.hidden-comment h3').click(function(){
			$(this).next('div').slideToggle();
		});
	});
</script>
<div class="full-news">
	<div class="left-item" style="width: 655px; padding: 10px 15px;">
		<h1>{title}</h1>
		<div class="full-news-right">
			<div class="ttfav"></div>
			<div style="clear: both; height: 10px;"></div>
			<div class="full-news-info">Категория: {link-category}</div>
			<div class="full-news-info">Автор: {author}</div>
			<div class="full-news-info">Рецепт добавлен: {date=d F Y}</div>
		</div>
		<div style="clear: both; height: 30px;"></div>
		<div class="full-news-content">
			{full-story}
			<div style="clear: both;"></div>
		</div>
		[edit]<img src="/Assets/Images/edit.png" />[/edit]

		<div style="clear: both;height: 20px;"></div>

		<div class="fav-block">{favorites}</div>
		<div class="print-block">[print-link]Распечатать рецепт[/print-link]</div>
		<div class="hidden-comment">
			<h3>Написать отзыв о рецепте</h3>
			<div class="hidden-comment-form">
				{addcomments}
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
	[related-news]
	<div class="left-item">
		<div class="left-item-title">ПОХОЖИЕ РЕЦЕПТЫ</div>
		<div class="left-item-content">
			<div class="caroufredsel_wrapper" style="text-align: left; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; width: 680px; height: 235px; margin: 0px; overflow: hidden;"><ul id="foo3" style="text-align: left; float: none; position: absolute; top: 0px; left: 0px; margin: 0px; width: 2380px; height: 235px;">
				{related-news}
			</ul></div>
			<div class="clearfix"></div>
			<a id="prev3" class="prev1" href="#" style="left: auto; top: -40px; right: 40px; display: block;"></a>
			<a id="next3" class="next1" href="#" style="left: auto; top: -40px; right: 10px; display: block;"></a>
			<div style="clear: both;height: 10px;"></div>
		</div>
	</div>
	[/related-news]
	{comments}
</div>