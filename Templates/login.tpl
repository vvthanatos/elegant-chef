{if $signedin}
<div class="avtorization">
	<div class="left-block left-block2">
		<div class="left-block-content">
			<div class="ava-cab">
				<div class="login-ava">
					<img src="{$foto}" />
				</div>
			</div>
			<div class="l-link">
				{if $admin-link}<a href="{$admin-link}" target="blank_" class="admin-link">Админцентр</a>{/if}
				<!--<a href="{addnews-link}" class="add-link">Опубликовать</a>-->
				<a href="{$profile-link}" class="profile-link">Профиль</a>
				<a href="{$pm-link}" class="pm-link">Сообщения ({$new-pm} | {$all-pm})</a>
				<a href="{$favorites-link}" class="favorites-link">Избранное ({$favorite-count})</a>
				<a href="{$logout-link}" class="lu-link">выйти</a>
				<div style="clear: both;"></div>
		</div>
		</div>
		<div style="clear: both;"></div>
	</div>
</div>
{else}
<div class="avtorization">
  <div class="left-block">
    <div class="left-block-content">
      <form method="post" action="">
        <div class="login-line">
          <input name="login_name" type="text" class="login-input-text" title="Ваше имя на сайте" />
        </div>
        <div class="login-line">
          <input name="login_password" type="password" class="login-input-text" title="Ваш пароль" />
        </div>
        <div style="clear: both;"></div>
        <input onclick="submit();" type="submit" class="enter"  value="войти" />
        <input name="login" type="hidden" id="login" value="submit" />
        <div class="reg-link">
          <a href="{$registration-link}" title="регистрация на сайте">Открыть свою кухню</a>
          <a href="{$lostpassword-link}" title="регистрация на сайте">Сделать дубликат ключей</a>
        </div>
      </form>
    </div>
    <div class="left-block-bottom"></div>
  </div>
</div>
{/if}