<!DOCTYPE html>
<html lang="ru">
<head>
	<link rel="stylesheet" href="/Assets/Css/style.css" type="text/css" />

	<script type="text/javascript" src="/Assets/Js/shareTT.js"></script>
	<script type="text/javascript" src="/Assets/Js/active.js"></script>
	<script type="text/javascript" src="/Assets/Js/jquery.carouFredSel-5.6.4-packed.js"></script>

  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
  <div class="header">
    <a href="/" class="logo">
      <img src="/Assets/Images/logo.png" alt="logo" />
    </a>
  </div>

  <div class="top-menu">
    <div>
      <a href="/">Главная</a>
      <a href="#">Рецепты</a>
      <a href="#">Новости кулинарии</a>
      <a href="#">Статьи</a>
      <a href="#">Видео рецепты</a>
      <a href="/?do=feedback">Обратная связь</a>
    </div>
    <div>
      <form method="post" action="" class="search-block">
        <input type="hidden" name="do" value="search">
        <input type="hidden" name="subaction" value="search" />
        <input type="text"   name="story" class="form-text" id="story" value="введите любимое блюдо" onblur="if(this.value=='') this.value='введите любимое блюдо';" onfocus="if(this.value=='введите любимое блюдо') this.value='';" title="наберите Ваш запрос и нажмите enter" />
        <input type="image" src="/Assets/Images/search.png" value="Найти!" class="form-search" alt="Найти!">
      </form>
    </div>
  </div>

  <div class="main">
    <div class="left-col">
      {if $page!=main}
      <div class="left-item" style="width: 685px; padding: 10px 0;">
        {$info}
        {$content}
      </div>
      {else}
      <div class="left-item">
        <div class="slider">
          <div class="slider-image">
            <a href="{$slider.full-link}">
              <img src="{$slider.image}" alt="{$slider.title}" title="{$slider.title}" />
            </a>
          </div>
          <div class="slider-news">
            <div class="slider-news-title">
              <span>РЕЦЕПТЫ НЕДЕЛИ</span>
              <img src="/Assets/Images/slider-title.png" />
            </div>
            {foreach $slider_news as $sn}
            <div class="slider-news-mini">
              <div class="slider-news-mini-title">{$sn.title}</div>
            </div>
            {/foreach}
          </div>
        </div>
      </div>

      <div class="left-item">
        <div class="left-item-title">ВЫБОР РЕДАКЦИИ<a href="#" class="all">показать все рекомендации</a></div>
        <div class="left-item-content">
          <div class="redaction-news-wrapper">
            {foreach $redaction_news as $receipt}
              <div class="redaction-news">
                <div class="redaction-news-image">
                  <a href="{$receipt.full-link}"><img src="{$receipt.image}" alt="{$receipt.title}"></a>
                </div>
                <h2><a href="{$receipt.full-link}">{$receipt.title}</a></h2>
                <div class="redaction-news-content">
                  {$receipt.description}
                </div>
                {if $receipt.ingreeds}
                  <div class="recept">
                    <div class="ingridien">
                      <div class="ingridient-title">Ингредиенты</div>
                      <div class="ingridient-content">
                        {$receipt.ingreeds}
                      </div>
                    </div>
                  </div>
                {/if}
              </div>
            {/foreach}
          </div>
        </div>
      </div>

      <div class="left-item">
        <div class="left-item-title">
          ВИДЕО РЕЦЕПТЫ <a href="#" class="all">показать все видео рецепты</a>
        </div>
        <div class="left-item-content">
          <ul id="foo1">
            <div class="video-news-wrapper">
              {foreach $video_receipts as $receipt}
                <div class="video-news">
                  <div class="video-news-image">
                    <img src="{$receipt.image}" alt="{$receipt.title}" />
                  </div>
                  <div class="video-news-bg"></div>
                  <div class="video-news-link-img"><a href="{$receipt.full-link}"></a></div>
                  <h2><a href="{$receipt.full-link}">{$receipt.title}</a></h2>
                </div>
              {/foreach}
            </div>
          </ul>
          <a id="prev1" class="prev1" href="#"></a>
          <a id="next1" class="next1" href="#"></a>
        </div>
      </div>

      <div class="left-item">
        <div class="left-item-title">НОВЫЕ РЕЦЕПТЫ НА САЙТЕ</div>
        <div class="left-item-content">
          <ul id="foo2">
            <div class="main-news-wrapper">
              {foreach $new_receipts as $receipt}
                <div class="bg-main-news">
                  <div class="main-news">
                    <div class="main-news-image">
                      <a href="{$receipt.full-link}"><img src="{$receipt.image}" alt="{$receipt.title}" /></a>
                    </div>
                    <div class="main-news-info">
                      <h2><a href="{$receipt.full-link}">{$receipt.title}</a></h2>
                      <div class="main-news-content">
                        {$receipt.description}
                      </div>
                    </div>
                    <div class="main-news-hover">
                      <h2><a href="{$receipt.full-link}">{$receipt.title}</a></h2>
                      <div class="main-news-hover-content">
                        {$receipt.description}
                      </div>
                      <div class="main-news-hover-more"><a href="{$full-link}">приготовить</a></div>
                    </div>
                  </div>
                </div>
              {/foreach}
            </div>
          </ul>
          <a id="prev2" class="prev1" href="#" style="left: 10px; bottom: 5px; top: auto;"></a>
          <a id="next2" class="next1" href="#" style="left: 40px; bottom: 5px; top: auto;"></a>
        </div>
      </div>
      {/if}
    </div>

    <div class="right-col">
      <div class="right-item">
        <div class="right-item-title">
          <img src="/Assets/Images/strelka-left.png" alt="strelka-left" />
            Что приготовить?
          <img src="/Assets/Images/strelka-right.png" alt="strelka-right" />
        </div>
        <div class="right-item-content">
          <div class="menu-block">
            <div class="menu-item">
              <img src="/Assets/Images/menu1.png" alt="menu" />
              <a href="#" class="menu-main-link">Первые блюда</a>
              <a href="#">борщи</a>,
              <a href="#">супы</a>,
              <a href="#">уха</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu2.png" alt="menu" />
              <a href="#" class="menu-main-link">Вторые блюда</a>
              <a href="#">мясо</a>,
              <a href="#">рыба</a>,
              <a href="#">птица</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu3.png" alt="menu" />
              <a href="#" class="menu-main-link">Гарниры</a>
              <a href="#">овощи</a>,
              <a href="#">мучное</a>,
              <a href="#">каши</a>,
              <a href="#">соусы</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu4.png" alt="menu" />
              <a href="#" class="menu-main-link">Салаты</a>
              <a href="#">мясные</a>,
              <a href="#">рыбные</a>,
              <a href="#">овощные</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu5.png" alt="menu" />
              <a href="#" class="menu-main-link">Выпечка</a>
              <a href="#">торты</a>,
              <a href="#">хлеб</a>,
              <a href="#">пироги</a>,
              <a href="#">печенье</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu6.png" alt="menu" />
              <a href="#" class="menu-main-link">Десерты</a>
              <a href="#">фрукты</a>,
              <a href="#">мороженное</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu7.png" alt="menu" />
              <a href="#" class="menu-main-link">Закуски</a>
              <a href="#">бутерброды</a>,
              <a href="#">нарезка</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu8.png" alt="menu" />
              <a href="#" class="menu-main-link">Пицца</a>
              <a href="#">мясная</a>,
              <a href="#">рыбная</a>,
              <a href="#">овощная</a>
            </div>
            <div class="menu-item">
              <img src="/Assets/Images/menu9.png" alt="menu" />
              <a href="#" class="menu-main-link">Напитки</a>
              <a href="#">коктейли</a>,
              <a href="#">компоты</a>,
              <a href="#">соки</a>
            </div>
          </div>
        </div>
      </div>
      <div class="right-item" style="background: #fcfbfa;">
        <div class="right-item-title">
          <img src="/Assets/Images/strelka-left.png" alt="strelka-left" />
            Ваша кухня у нас
          <img src="/Assets/Images/strelka-right.png" alt="strelka-right" />
        </div>
        <div class="right-item-content">
          {include file="login.tpl"}
        </div>
      </div>
      <div class="right-item">
        <div class="right-item-title">
          <img src="/Assets/Images/strelka-left.png" alt="strelka-left" />
            Популярные рецепты
          <img src="/Assets/Images/strelka-right.png" alt="strelka-right" />
        </div>
        <div class="right-item-content">
          <div class="top-news-wrapper">
            {foreach $rop_receipts as $receipt}
              <div class="top-news-block">
                <div class="top-news-block-image">
                  <a href="{$receipt.link}">
                    <img src="{$receipt.image}" alt="{$receipt.title}" />
                  </a>
                </div>
                <a href="{$receipt.link}" class="top-news-title">{$receipt.title}</a>
              </div>
            {/foreach}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer">
    <div class="logo2">
      <a href="/"><img src="/Assets/Images/logo2.png" alt="" title="" /></a>
      <div>
        На нашем сайте собраны рецепты со всего мира, любимые и проверенные многими поколениями. У нас можно найти классические рецепты блюд из мяса и
        рыбы , блюда национальных кухонь, праздничные блюда, салаты и супы и бесчисленное множество рецептов десертов и выпечки. Видеорецепты наглядно
        продемонстрируют вам секреты приготовления блюд от лучших кулинаров мира.
      </div>
      <div class="count">
        <img src="/Assets/Images/count.png" />
      </div>
    </div>
    <div class="copyright">
      Copyright © 2013-2016 <a href="/">Elegant-Chef.com</a>
    </div>
  </div>

  {literal}
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-44455183-1', 'auto');
    ga('send', 'pageview');
  </script>
  {/literal}
</body>
</html>