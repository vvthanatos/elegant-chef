<div class="redaction-news">
	<div class="redaction-news-image">
		<a href="{$link}"><img src="{$image-1}" alt="{$title}"></a>
	</div>
	<h2><a href="{$link}">{$title}</a></h2>
	<div class="redaction-news-content">
		{$text}
		<div style="clear: both;"></div>
	</div>
</div>