[searchposts]
[fullresult]
<div class="bg-main-news">
	<div class="main-news">
		<div class="main-news-image">
			<a href="{full-link}"><img src="{image-1}" alt="{title}" /></a>
		</div>
		<div class="main-news-info">
			<h2><a href="{full-link}">{title}</a></h2>
			<div class="main-news-content">
				{short-story limit="250"}<div style="clear: both;"></div>
			</div>
		</div>
		<div class="main-news-hover">
			<h2><a href="{full-link}">{title}</a></h2>
			<div class="main-news-hover-content">
				{short-story limit="250"}<div style="clear: both;"></div>
			</div>
			<div class="main-news-hover-more"><a href="{full-link}">приготовить</a></div>
		</div>
	</div>
</div>
[/fullresult]
[shortresult]
<div class="s-result">
	[full-link]{title}[/full-link]
</div>
[/shortresult]
[/searchposts]
[searchcomments]
[fullresult]
<div class="left-item">
<div class="left-item-content" >
<div class="comment-block">
	<div class="comment-block-left">
		<div class="hidden-coment"><img src="{foto}" alt=""></div>
	</div>
	<div class="comment-block-right">
		<div class="comment-block-right2">
				<div class="comment-text">
					<div class="comment-text-title">
						<strong class="author">{author}</strong> написал:
					</div>
					{comment}[signature]<br /><br />--------------------<br /><div class="slink">{signature}</div>[/signature]<br />
					<div class="comment-text-more">{mass-action}[fast]цитировать[/fast] [spam]спам[/spam] [com-edit]изменить[/com-edit] [com-del]удалить[/com-del] [complaint]жалоба[/complaint]</div>
				</div>				
		</div>
	</div>
</div>
<div style="clear: both;height: 10px;"></div>
</div>
</div>
[/fullresult]
[shortresult]
<div class="left-item">
<div class="left-item-content" >
<div class="comment-block">
	<div class="comment-block-left">
		<div class="hidden-coment"><img src="{foto}" alt=""></div>
	</div>
	<div class="comment-block-right">
		<div class="comment-block-right2">
				<div class="comment-text">
					<div class="comment-text-title">
						<strong class="author">{author}</strong> написал:
					</div>
					{comment}[signature]<br /><br />--------------------<br /><div class="slink">{signature}</div>[/signature]<br />
					<div class="comment-text-more">{mass-action}[fast]цитировать[/fast] [spam]спам[/spam] [com-edit]изменить[/com-edit] [com-del]удалить[/com-del] [complaint]жалоба[/complaint]</div>
				</div>				
		</div>
	</div>
</div>
<div style="clear: both;height: 10px;"></div>
</div>
</div>
[/shortresult]
[/searchcomments]