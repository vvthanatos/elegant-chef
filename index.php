<?php
include ('Vendor/smarty/Smarty.class.php');

$smarty = new Smarty();
$smarty->setTemplateDir('Templates/');
$smarty->setCompileDir('Templates_c/');

$receipts = [
  1 => [
    "title" => "Грог по-рыбацки",
    "description" => "Чай залить кипятком, дать настояться 5 минут, после чего процедить и добавить коньяк, ром, сок и корку лимона. По вкусу подсластить полученную смесь медом и размешать. Разлить в подогретые чашки и сразу подать.",
    "ingreeds" => "Чай - 6 ч.л. Вода - 2 стакана Коньяк - 125 мл Ром - 1 рюмка Корка половины лимона Сок из 2 лимонов Мед - по вкусу ",
    "image" => "/Assets/Posts/post1.jpg",
  ],
  2 => [
    "title" => "Грог южный",
    "description" => "В воду всыпать ванильный сахар, семена аниса, сушеную мяту, добавить цедру лимона, сахарный песок, размешать и кипятить около 5 минут. Затем процедить, добавить ром, ананасовый сок, размешать горячим разлить в подогретые чашки.",
    "ingreeds" => "Вода - 2 стакана Ванильный сахар - 15г Семена аниса - 5г Сахар - 1 ст. л. Цедра одного лимона Сухая мята (листья) - 1 ст. л. Сахар - 1 ст. л. Ром - 750 мл Ананасовый сок - 1 стакан",
    "image" => "/Assets/Posts/post2.jpg",
  ],
  3 => [
    "title" => "Анисовка (старинный рецепт)",
    "description" => "100 г аниса стирают в мелкий порошок и в течение месяца настаивают в 5-6 л разведенного до 25-30%спирта. Затем смесь перегоняют, доводят до крепости 45% и разливают в бутылки.",
    "ingreeds" => "- анис (мелкий порошок) - 100г - спирт 25-30% - 5-6 л. ",
    "image" => "/Assets/Posts/post3.jpg",
  ],
  4 => [
    "title" => "Настойка зверобоя",
    "description" => "Траву зверобоя с цветами заливают спиртом, настаивают 1 неделю, процеживают и пьют по 1 ст. ложке 3 раза в день при простуде.",
    "ingreeds" => "Зверобой - 100г Спирт - 1/2 л. ",
    "image" => "/Assets/Posts/post4.jpg",
  ],
  5 => [
    "title" => "Апельсиновая настойка",
    "description" => "Корки и корень кладут в бутыль, заливают водкой, настаивают две недели, затем процеживают через фильтровальную бумагу.",
    "ingreeds" => "",
    "image" => "/Assets/Posts/post5.jpg",
  ],
];

$smarty->assign('page', 'main');
$smarty->assign('slider', [
  "full-link" => "/",
  "image" => $receipts[1]["image"],
  "title" => $receipts[1]["title"],
]);
$smarty->assign('slider_news', array_slice($receipts, 0, 5));
$smarty->assign('redaction_news', array_slice($receipts, 0, 5));
$smarty->assign('video_receipts', array_slice($receipts, 0, 3));
$smarty->assign('new_receipts', array_slice($receipts, 0, 4));
$smarty->assign('rop_receipts', array_slice($receipts, 0, 3));

$smarty->display('main.tpl');